# notes

This is a Notes App made using the Flutter Framework.

It lets the user create, edit and delete notes.

It makes use of Firebase Firestore as backend to store all the data.

![notes1](/uploads/e9eb1a19983eb4d4719a7db2d5984062/notes1.mp4)

![notes2](/uploads/f9fedbbe0289309e310d46de0b379387/notes2.mp4)
